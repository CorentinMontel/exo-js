// Tu travail sur un logiciel pour les profs/eleves qui centralise les notes des eleves.
// Certains professeurs notent sur 10 et d'autres sur 20, mais tout ton systeme est basé sur une notation sur 20
//
// Tu doit créer une fonction qui transformes les notes attribués sur 10 en des notes sur 20
// Exemple : Elisabeth a 9/10, sa note sur 20 est 18
// Ta fonction prend en entré un tableau de notes sur 10 que tu doit passer à 20

/**
 *
 * @param {int[]} notes
 */
function transformBase20(notes) {
    // Your code here
}

// Resolution : DON'T TOUCH BELOW !!!
function loadExo() {
    return {
        name: "Sur 20 !",
        description: "Tu travail sur un logiciel pour les profs/eleves qui centralise les notes des eleves.\n" +
            "Certains professeurs notent sur 10 et d'autres sur 20, mais tout ton systeme est basé sur une notation sur 20\n" +
            "\n" +
            "Tu doit créer une fonction qui transformes les notes attribués sur 10 en des notes sur 20\n" +
            "Exemple : Elisabeth a 9/10, sa note sur 20 est 18\n" +
            "Ta fonction prend en entré un tableau de notes sur 10 que tu doit passer à 20",
        fn: transformBase20,
        returnType: Array,
        file: "array/exo1.js",
        tests: [
            [[0, 1, 2], [0, 2, 4]],
            [[4, 9, 5, 7], [8, 18, 10, 14]]
        ]
    }
}