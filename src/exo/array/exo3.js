// Tu travail toujours sur un logiciel pour les profs/eleves qui centralise les notes des eleves.
// Ton logiciel calcul les moyenne des eleves en fin de trimestre
//
// Tu doit créer une fonction qui donne la moyenne d'un eleves à partir des notes qu'il a eu
// Exemple : Elisabeth a 15, 10 et 20, la fonction retournera 15
// Ta fonction prend en entré un tableau de notes et retourne un float
// Rappel : Moyenne = Somme des notes / nombre de notes

/**
 *
 * @param {int[]} notes
 */
function meanNotes(notes) {
    // Tour code here
}

// Resolution : DON'T TOUCH BELOW !!!
function loadExo() {
    return {
        name: "Moyenne",
        description: "Tu travail toujours sur un logiciel pour les profs/eleves qui centralise les notes des eleves.\n" +
            "Ton logiciel calcul les moyenne des eleves en fin de trimestre\n" +
            "\n" +
            "Tu doit créer une fonction qui donne la moyenne d'un eleves à partir des notes qu'il a eu\n" +
            "Exemple : Elisabeth a 15, 10 et 20, la fonction retournera 15\n" +
            "Ta fonction prend en entré un tableau de notes et retourne un float\n" +
            "Rappel : Moyenne = Somme des notes / nombre de notes",
        fn: meanNotes,
        returnType: Number,
        file: "array/exo3.js",
        tests: [
            [[0, 1, 2], 1],
            [[8, 18, 10, 14], 12.5]
        ]
    }
}