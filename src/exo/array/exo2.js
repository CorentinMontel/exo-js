// Tu travail toujours sur un logiciel pour les profs/eleves qui centralise les notes des eleves.
// Pour afficher le bilan de l'année, on présente les notes des eleves par ordre décroissant.
//
// Tu doit créer une fonction qui classe les notes de la meilleur à la moins bonne
// Exemple : Elisabeth a 15, 9 et 18, la fonction retournera [18, 15, 9]
// Ta fonction prend en entré un tableau de notes et retourne un tableau de note

/**
 *
 * @param {int[]} notes
 */
function sortNotes(notes) {
    // Your code here
}

// Resolution : DON'T TOUCH BELOW !!!
function loadExo() {
    return {
        name: "Classement décroissant",
        description: "Tu travail toujours sur un logiciel pour les profs/eleves qui centralise les notes des eleves.\n" +
            "Pour afficher le bilan de l'année, on présente les notes des eleves par ordre décroissant.\n" +
            "\n" +
            "Tu doit créer une fonction qui classe les notes de la meilleur à la moins bonne\n" +
            "Exemple : Elisabeth a 15, 9 et 18, la fonction retournera [18, 15, 9]\n" +
            "Ta fonction prend en entré un tableau de notes et retourne un tableau de note",
        fn: sortNotes,
        returnType: Array,
        file: "array/exo2.js",
        tests: [
            [[0, 1, 2], [2,1,0]],
            [[8, 18, 10, 14], [18, 14, 10, 8]]
        ]
    }
}
