function arrayEquals(a, b) {
    return Array.isArray(a) &&
        Array.isArray(b) &&
        a.length === b.length &&
        a.every((val, index) => val === b[index]);
}

function testResolution(exo) {
    const result = {
        success: true,
        testsPerformed: 0,
        testFailed: []
    }

    for (const test of exo.tests) {
        const testResult = exo.fn(test[0])

        if (exo.returnType === Array) {
            if (! Array.isArray(testResult)) {
                console.warn("The result should be an array " + typeof testResult + " given")
            }
            if (! arrayEquals(testResult,test[1])) {
                console.warn("Fail !")
                console.log("Expected :", test[1])
                console.log("Given :", testResult)
                result.success = false
            }
        } else {
            if (testResult !== test[1]) {
                console.warn("Fail !")
                console.log("Expected :", test[1])
                console.log("Given :", testResult)
                result.success = false
            }
        }
    }

    return result
}